# AngularFB

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 8.0.3.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).


## Firebase configuration

Run `ng add @angular/fire` (https://www.npmjs.com/package/@angular/fire)
The official Angular library for Firebase.

## Add Firebase to your web app using config object
https://fireship.io/snippets/install-angularfire/
changed the setting of database ==> Database-> Rules   (allow read, write: if true;)

## Refer Firebase related solution
https://fireship.io/snippets
https://www.npmjs.com/package/@angular/fire





