export class StudentProfile {
    name: String;
    gender: String;
    age: number;
}

export class Student {
    gender: String[] = ['Male', 'Female'];
    age: number[] = [21, 22, 23, 24, 25];
}

export class StudentAddress {
    docId: string;
    city: String;
    state: String;
    country: String;
}

export class Address {
    city: String[] = ['chennai', 'madurai', 'munnar', 'cochi', 'london', 'new york' ];
    state: String[] = ['Tamilnadu', 'Kerala', 'United State'];
    country: String[] = ['India', 'US']
}