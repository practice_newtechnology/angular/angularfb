import { Component, OnInit } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { StudentProfile, StudentAddress, Student, Address } from './serviceFile'
import { FormGroup, FormControl, FormBuilder } from '@angular/forms';
import * as firebase from 'firebase';


@Component({
  selector: 'app-firestore',
  templateUrl: './firestore.component.html',
  styleUrls: ['./firestore.component.scss']
})
export class FirestoreComponent implements OnInit {
  public gender; public age;
  public city; public state; public country;
  public studentForm: FormGroup;
  public addressForm: FormGroup;
  public studentProfile: StudentProfile;
  public studentAddress: StudentAddress;

  constructor(private db: AngularFirestore, private formBuilder: FormBuilder) {
    let student = new Student();
    let address = new Address();

    this.gender = student.gender;
    this.age = student.age;
    this.city = address.city;
    this.state = address.state;
    this.country = address.country;

    this.studentForm = this.formBuilder.group({
      name: [''],
      gender: [''],
      age: ['']
    });
    this.addressForm = this.formBuilder.group({
      docId: [''],
      city: [''],
      state: [''],
      country: ['']
    });
  
  }

  ngOnInit() {
  }
  
  onSubmitStudent() {
    this.studentProfile = new StudentProfile();
    this.studentProfile = this.studentForm.value;
    console.log(this.studentProfile);
  }

  onSubmitAddress() {
    this.studentAddress = new StudentAddress();
    this.studentAddress = this.addressForm.value;
    console.log(this.studentAddress);
  }

  /*  1. If studentProfile collection is not there, create a new collection with the name `studentProfile` and a document with auto id
      2. If studentProfile collection is there, added new document with auto-id under the `studentProfile`
      3. Firestore generates auto-ID for the document
      var studentDocRef = db.collection("cities").doc(); // generate auto-id and we can set the data later
  */
  addStudentProfile() {
    this.db.collection("studentProfile").add(this.studentProfile)
    .then(function(studentDocRef) {
      console.log(studentDocRef);
      console.log("Document written with ID: ", studentDocRef.id);
    })
    .catch(function(error) {
      console.error("Error adding document: ", error);
    });
  }

  /*  get() ==> to retrieve the entire collection `studentProfile`
  */
  readStudentProfile() {
    this.db.collection("studentProfile").get().subscribe((querySnapshot) => {
      querySnapshot.forEach((doc) => {
        console.log(doc);
        console.log(doc.id);
        console.log(doc.data());
      });
    });
  }

  /*  1. If the document does not exist, it will be created
      2. If the document does exist, its contents will be overwritten with the newly provided data
      3. you must specify an ID `this.studentAddress.docId` for the document  
  */
  setStudentAddress() {
    this.db.collection("studentAddress").doc(this.studentAddress.docId).set(this.studentAddress)
    .then(function() {
      console.log("Document successfully written!");
    })
    .catch(function(error) {
      console.error("Error writing document: ", error);
    });
  }

  /*  To update specific fields of a document without overwriting the entire document `studentProfile`
   */
  updateStudentProfile() {
    this.db.collection("studentProfile").get().subscribe((querySnapshot) => {
      querySnapshot.forEach((doc) => {
        if(doc.data().name === 'Logesh') {
          let docRef = this.db.collection("studentProfile").doc(doc.id);
          console.log(docRef);
          docRef.update({
            name: this.studentForm.value.name
          })
          .then(function() {
              console.log("Document successfully updated!");
          })
          .catch(function(error) {
              // The document probably doesn't exist.
              console.error("Error updating document: ", error);
          });      
        }
      });
    });
  }

  updateStudentAddress() {
    this.db.collection("studentAddress").get().subscribe((querySnapshot) => {
      querySnapshot.forEach((doc) => {
        if(doc.data().city === 'madurai') {
          console.log(doc.id);
          let docRef = this.db.collection("studentAddress").doc(doc.id);
          console.log(docRef);
          docRef.update({
            city: this.addressForm.value.city
          })
          .then(function() {
              console.log("Document successfully updated!");
          })
          .catch(function(error) {
              // The document probably doesn't exist.
              console.error("Error updating document: ", error);
          });      
        }
      });
    });
  }

  deleteCityOfStudentAddress() {
    this.db.collection('studentAddress').doc('logesh').update({
      city: firebase.firestore.FieldValue.delete()
    });
  }

  deleteDocumentStudentAddress() {
    this.db.collection("studentAddress").doc("logesh").delete().then(function() {
      console.log("Document successfully deleted!");
    }).catch(function(error) {
        console.error("Error removing document: ", error);
    });
  }

}
