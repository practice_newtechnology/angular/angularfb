import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FirestorageComponent } from './firestorage.component';

describe('FirestorageComponent', () => {
  let component: FirestorageComponent;
  let fixture: ComponentFixture<FirestorageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FirestorageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FirestorageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
