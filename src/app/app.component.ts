import { Component } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'AngularFB';

  // constructor(private db: AngularFirestore) {
  //   const studentProfile = db.collection('studentProfile').valueChanges();
  //   studentProfile.subscribe(console.log);
  // }
}