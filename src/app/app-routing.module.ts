import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { FirestoreComponent } from './firestore/firestore.component';
import { FirestorageComponent } from './firestorage/firestorage.component';

const routes: Routes = [
  { path: '', component: HomeComponent },
  { path: 'firestore', component: FirestoreComponent },
  { path: 'firestorage', component: FirestorageComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
